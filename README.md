# Sumário

- [**O que é**](#o-que-é)
- [**Exemplo 01**](#exemplo-01)
    - [Container](#container)
    - [Publisher](#publisher)
    - [Consumer](#consumer)
    - [Executando](#executando)
- [**Referência**](#referência)


# O que é

O rabbitMQ é um sistema de fila de processamento, ele funciona no esquema de pub/sub, então eu recomendo que você leia o conteúdo de [pub/sub](https://gitlab.com/e3666/backend-engineer/publish-subscribe) antes de seguir com esse material, pois não explicarei com muitos detalhes aqui.

A arquitetura de um sistama que utiliza o rabbitMQ é a seguinte:

<img src='./images/fig_01.svg'/>

# Exemplo 01

[Arquivos aqui...](./exemplo-01)

Vou utilizar o RabbitMQ em um container, e então vou criar um publisher e três subscribers, tudo em python, mas para facilitar a maleabilidade de execução, vou deixar os dois python fora do container, e irei instalar tudo em venv. A estrutura do projeto fica sendo a seguinte:

<img src='./images/fig_02.svg'/>

## Container

Para construir o container do RabbitMQ basta usar a imagem oficial, e habilitar a porta 5672

```yml
version: "3"

services: 
    rabbitmq:
        image: rabbitmq
        ports: 
            - "5672:5672"
```

## Publisher

```py
import pika
import json
import sys

queue_name = sys.argv[1]

connection = pika.BlockingConnection(pika.ConnectionParameters('localhost'))
channel = connection.channel()

channel.queue_declare(queue=queue_name)

my_body = {'a': 45, 'b': 78}

channel.basic_publish(exchange='',
                      routing_key=queue_name,
                      body=json.dumps(my_body))

print(f"Enviado com sucesso para a fila: {queue_name}")

connection.close()
```

- O código simplesmente faz a conexão no localhost (a porta do rabbitMQ já é padrão, então não precisa ser informada)
- Crio a fila (caso ela já exista, não acontece nada), veja que o nome da fila vem por parâmetro
- Envio o dicionário em formáto de string para essa queue
- Encerro a conexão

## Consumer

```py
import sys
import pika

queue_name = sys.argv[1]

connection = pika.BlockingConnection(pika.ConnectionParameters(host='localhost'))
channel = connection.channel()

channel.queue_declare(queue=queue_name)

def callback(ch, method, properties, body):
    print(f"Recebi {body}")

channel.basic_consume(queue=queue_name, on_message_callback=callback, auto_ack=True)

print(f'Aguardando mensagem da fila: {queue_name}')
channel.start_consuming()
```

- Se conecta ao rabbitMQ
- Definimos uma função de callback, que será executada quando o rabbitMQ enviar algo
- O método basic_consume é um loop infinito, que fica esperando algo da fila que esse consumer utiliza, ou seja, a queue_name, que vai ser passada por argumento

> OBS.: O parâmetro auto_ack, é para informar que aquele processo listado foi consumido, assim o rabbitMQ remove ele da fila, caso contrário ele vai ficar lá

## Executando

Vou deixar três consumers ativos, e vou executar o publisher na mão, e a cada hora vou mandar por parâmetro a fila que quero publicar, veja tudo funcionando no gif abaixo

<img src='./images/fig_03.gif'/>

Agora vou fazer outro experimento, que é colocar dados na fila, com o consumidor desligado, para mostrar que os dados não se perdem, mas que eles são enviados ao consumidor assim que ele é religado.

<img src='./images/fig_04.gif'/>


# Referência

- [RabbitMQ Crash Course](https://www.youtube.com/watch?v=Cie5v59mrTg&list=PLQnljOFTspQWGuRmwojJ6LiV0ejm6eOcs&index=20)

- [RabbitMQ - Hello World - Python](https://www.rabbitmq.com/tutorials/tutorial-one-python.html)