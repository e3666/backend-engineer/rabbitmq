import sys
import pika

queue_name = sys.argv[1]

connection = pika.BlockingConnection(pika.ConnectionParameters(host='localhost'))
channel = connection.channel()

channel.queue_declare(queue=queue_name)

def callback(ch, method, properties, body):
    print(f"Recebi {body}")

channel.basic_consume(queue=queue_name, on_message_callback=callback, auto_ack=True)

print(f'Aguardando mensagem da fila: {queue_name}')
channel.start_consuming()