import pika
import json
import sys

queue_name = sys.argv[1]

connection = pika.BlockingConnection(pika.ConnectionParameters('localhost'))
channel = connection.channel()

channel.queue_declare(queue=queue_name)

my_body = {'a': 45, 'b': 78}

channel.basic_publish(exchange='',
                      routing_key=queue_name,
                      body=json.dumps(my_body))

print(f"Enviado com sucesso para a fila: {queue_name}")

connection.close()
